#!/bin/bash

#./build-all.sh

LAST_COMMIT_ID=`git log --format="%h" -n 1`

docker login registry.gitlab.com/pbsa/peerplays-environment

#===============================================================================
# Ubuntu base images
#===============================================================================

#docker tag ubuntu-for-peerplays-18-04 registry.gitlab.com/pbsa/peerplays-environment/ubuntu-for-peerplays-18-04:$LAST_COMMIT_ID
#docker tag ubuntu-for-peerplays-18-04 registry.gitlab.com/pbsa/peerplays-environment/ubuntu-for-peerplays-18-04:latest
#docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/ubuntu-for-peerplays-18-04

docker tag ubuntu-for-peerplays-20-04 registry.gitlab.com/pbsa/peerplays-environment/ubuntu-for-peerplays-20-04:$LAST_COMMIT_ID
docker tag ubuntu-for-peerplays-20-04 registry.gitlab.com/pbsa/peerplays-environment/ubuntu-for-peerplays-20-04:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/ubuntu-for-peerplays-20-04

#docker tag ubuntu-for-peerplays-22-04 registry.gitlab.com/pbsa/peerplays-environment/ubuntu-for-peerplays-22-04:$LAST_COMMIT_ID
#docker tag ubuntu-for-peerplays-22-04 registry.gitlab.com/pbsa/peerplays-environment/ubuntu-for-peerplays-22-04:latest
#docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/ubuntu-for-peerplays-22-04

#===============================================================================
# Sidechain node images
#===============================================================================

docker tag bitcoin-for-peerplays registry.gitlab.com/pbsa/peerplays-environment/bitcoin-for-peerplays:$LAST_COMMIT_ID
docker tag bitcoin-for-peerplays registry.gitlab.com/pbsa/peerplays-environment/bitcoin-for-peerplays:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/bitcoin-for-peerplays

docker tag libbitcoin-server-for-peerplays registry.gitlab.com/pbsa/peerplays-environment/libbitcoin-server-for-peerplays:$LAST_COMMIT_ID
docker tag libbitcoin-server-for-peerplays registry.gitlab.com/pbsa/peerplays-environment/libbitcoin-server-for-peerplays:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/libbitcoin-server-for-peerplays

docker tag ethereum-for-peerplays registry.gitlab.com/pbsa/peerplays-environment/ethereum-for-peerplays:$LAST_COMMIT_ID
docker tag ethereum-for-peerplays registry.gitlab.com/pbsa/peerplays-environment/ethereum-for-peerplays:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/ethereum-for-peerplays

docker tag hive-for-peerplays registry.gitlab.com/pbsa/peerplays-environment/hive-for-peerplays:$LAST_COMMIT_ID
docker tag hive-for-peerplays registry.gitlab.com/pbsa/peerplays-environment/hive-for-peerplays:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/hive-for-peerplays

#===============================================================================
# Faucet
#===============================================================================

docker tag faucet-for-peerplays registry.gitlab.com/pbsa/peerplays-environment/faucet-for-peerplays:$LAST_COMMIT_ID
docker tag faucet-for-peerplays registry.gitlab.com/pbsa/peerplays-environment/faucet-for-peerplays:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/faucet-for-peerplays

#===============================================================================
# Peerplays deps images
#===============================================================================

#docker tag peerplays-deps-18-04 registry.gitlab.com/pbsa/peerplays-environment/peerplays-deps-18-04:$LAST_COMMIT_ID
#docker tag peerplays-deps-18-04 registry.gitlab.com/pbsa/peerplays-environment/peerplays-deps-18-04:latest
#docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays-deps-18-04

docker tag peerplays-deps-20-04 registry.gitlab.com/pbsa/peerplays-environment/peerplays-deps-20-04:$LAST_COMMIT_ID
docker tag peerplays-deps-20-04 registry.gitlab.com/pbsa/peerplays-environment/peerplays-deps-20-04:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays-deps-20-04

#docker tag peerplays-deps-22-04 registry.gitlab.com/pbsa/peerplays-environment/peerplays-deps-22-04:$LAST_COMMIT_ID
#docker tag peerplays-deps-22-04 registry.gitlab.com/pbsa/peerplays-environment/peerplays-deps-22-04:latest
#docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays-deps-22-04

#===============================================================================
# Peerplays base images
#===============================================================================

#docker tag peerplays-base-18-04 registry.gitlab.com/pbsa/peerplays-environment/peerplays-base-18-04:$LAST_COMMIT_ID
#docker tag peerplays-base-18-04 registry.gitlab.com/pbsa/peerplays-environment/peerplays-base-18-04:latest
#docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays-base-18-04

docker tag peerplays-base-20-04 registry.gitlab.com/pbsa/peerplays-environment/peerplays-base-20-04:$LAST_COMMIT_ID
docker tag peerplays-base-20-04 registry.gitlab.com/pbsa/peerplays-environment/peerplays-base-20-04:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays-base-20-04

#docker tag peerplays-base-22-04 registry.gitlab.com/pbsa/peerplays-environment/peerplays-base-22-04:$LAST_COMMIT_ID
#docker tag peerplays-base-22-04 registry.gitlab.com/pbsa/peerplays-environment/peerplays-base-22-04:latest
#docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays-base-22-04

#===============================================================================
# Peerplays images
#===============================================================================

docker tag peerplays-all-in-one registry.gitlab.com/pbsa/peerplays-environment/peerplays-all-in-one:$LAST_COMMIT_ID
docker tag peerplays-all-in-one registry.gitlab.com/pbsa/peerplays-environment/peerplays-all-in-one:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays-all-in-one

docker tag peerplays01 registry.gitlab.com/pbsa/peerplays-environment/peerplays01:$LAST_COMMIT_ID
docker tag peerplays01 registry.gitlab.com/pbsa/peerplays-environment/peerplays01:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays01

docker tag peerplays02 registry.gitlab.com/pbsa/peerplays-environment/peerplays02:$LAST_COMMIT_ID
docker tag peerplays02 registry.gitlab.com/pbsa/peerplays-environment/peerplays02:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays02

docker tag peerplays03 registry.gitlab.com/pbsa/peerplays-environment/peerplays03:$LAST_COMMIT_ID
docker tag peerplays03 registry.gitlab.com/pbsa/peerplays-environment/peerplays03:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays03

docker tag peerplays04 registry.gitlab.com/pbsa/peerplays-environment/peerplays04:$LAST_COMMIT_ID
docker tag peerplays04 registry.gitlab.com/pbsa/peerplays-environment/peerplays04:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays04

docker tag peerplays05 registry.gitlab.com/pbsa/peerplays-environment/peerplays05:$LAST_COMMIT_ID
docker tag peerplays05 registry.gitlab.com/pbsa/peerplays-environment/peerplays05:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays05

docker tag peerplays06 registry.gitlab.com/pbsa/peerplays-environment/peerplays06:$LAST_COMMIT_ID
docker tag peerplays06 registry.gitlab.com/pbsa/peerplays-environment/peerplays06:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays06

docker tag peerplays07 registry.gitlab.com/pbsa/peerplays-environment/peerplays07:$LAST_COMMIT_ID
docker tag peerplays07 registry.gitlab.com/pbsa/peerplays-environment/peerplays07:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays07

docker tag peerplays08 registry.gitlab.com/pbsa/peerplays-environment/peerplays08:$LAST_COMMIT_ID
docker tag peerplays08 registry.gitlab.com/pbsa/peerplays-environment/peerplays08:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays08

docker tag peerplays09 registry.gitlab.com/pbsa/peerplays-environment/peerplays09:$LAST_COMMIT_ID
docker tag peerplays09 registry.gitlab.com/pbsa/peerplays-environment/peerplays09:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays09

docker tag peerplays10 registry.gitlab.com/pbsa/peerplays-environment/peerplays10:$LAST_COMMIT_ID
docker tag peerplays10 registry.gitlab.com/pbsa/peerplays-environment/peerplays10:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays10

docker tag peerplays11 registry.gitlab.com/pbsa/peerplays-environment/peerplays11:$LAST_COMMIT_ID
docker tag peerplays11 registry.gitlab.com/pbsa/peerplays-environment/peerplays11:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays11

docker tag peerplays12 registry.gitlab.com/pbsa/peerplays-environment/peerplays12:$LAST_COMMIT_ID
docker tag peerplays12 registry.gitlab.com/pbsa/peerplays-environment/peerplays12:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays12

docker tag peerplays13 registry.gitlab.com/pbsa/peerplays-environment/peerplays13:$LAST_COMMIT_ID
docker tag peerplays13 registry.gitlab.com/pbsa/peerplays-environment/peerplays13:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays13

docker tag peerplays14 registry.gitlab.com/pbsa/peerplays-environment/peerplays14:$LAST_COMMIT_ID
docker tag peerplays14 registry.gitlab.com/pbsa/peerplays-environment/peerplays14:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays14

docker tag peerplays15 registry.gitlab.com/pbsa/peerplays-environment/peerplays15:$LAST_COMMIT_ID
docker tag peerplays15 registry.gitlab.com/pbsa/peerplays-environment/peerplays15:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays15

docker tag peerplays16 registry.gitlab.com/pbsa/peerplays-environment/peerplays16:$LAST_COMMIT_ID
docker tag peerplays16 registry.gitlab.com/pbsa/peerplays-environment/peerplays16:latest
docker push --all-tags registry.gitlab.com/pbsa/peerplays-environment/peerplays16

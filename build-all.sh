#!/bin/bash

#===============================================================================
# Ubuntu base images
#===============================================================================

#docker build -t ubuntu-for-peerplays-18-04 -f ./ubuntu/Dockerfile-18-04 ./ubuntu/
docker build -t ubuntu-for-peerplays-20-04 -f ./ubuntu/Dockerfile-20-04 ./ubuntu/
#docker build -t ubuntu-for-peerplays-22-04 -f ./ubuntu/Dockerfile-22-04 ./ubuntu/

#===============================================================================
# Sidechain node images
#===============================================================================

docker build -t bitcoin-for-peerplays -f ./bitcoin/bitcoind/Dockerfile.v0.22.0 ./bitcoin/bitcoind/

docker build -t libbitcoin-server-for-peerplays -f ./bitcoin/libbitcoin-server/Dockerfile ./bitcoin/libbitcoin-server/

docker build -t ethereum-for-peerplays -f ./ethereum/Dockerfile ./ethereum/

docker build -t hive-for-peerplays -f ./hive/Dockerfile.v1.27.0 ./hive/

#===============================================================================
# Faucet
#===============================================================================

docker build -t faucet-for-peerplays -f ./faucet/Dockerfile ./faucet/

#===============================================================================
# Peerplays deps images
#===============================================================================

#docker build -t peerplays-deps-18-04 -f ./peerplays/deps/Dockerfile-18-04 ./peerplays/deps/
docker build -t peerplays-deps-20-04 -f ./peerplays/deps/Dockerfile-20-04 ./peerplays/deps/
#docker build -t peerplays-deps-22-04 -f ./peerplays/deps/Dockerfile-22-04 ./peerplays/deps/

#===============================================================================
# Peerplays base images
#===============================================================================

#docker build -t peerplays-base-18-04 -f ./peerplays/Dockerfile-18-04 ./peerplays/
docker build -t peerplays-base-20-04 -f ./peerplays/Dockerfile-20-04 ./peerplays/
#docker build -t peerplays-base-22-04 -f ./peerplays/Dockerfile-22-04 ./peerplays/

#===============================================================================
# Peerplays images
#===============================================================================

docker build -t peerplays-all-in-one -f ./peerplays/Dockerfile00 ./peerplays/
docker build -t peerplays01 -f ./peerplays/Dockerfile01 ./peerplays/
docker build -t peerplays02 -f ./peerplays/Dockerfile02 ./peerplays/
docker build -t peerplays03 -f ./peerplays/Dockerfile03 ./peerplays/
docker build -t peerplays04 -f ./peerplays/Dockerfile04 ./peerplays/
docker build -t peerplays05 -f ./peerplays/Dockerfile05 ./peerplays/
docker build -t peerplays06 -f ./peerplays/Dockerfile06 ./peerplays/
docker build -t peerplays07 -f ./peerplays/Dockerfile07 ./peerplays/
docker build -t peerplays08 -f ./peerplays/Dockerfile08 ./peerplays/
docker build -t peerplays09 -f ./peerplays/Dockerfile09 ./peerplays/
docker build -t peerplays10 -f ./peerplays/Dockerfile10 ./peerplays/
docker build -t peerplays11 -f ./peerplays/Dockerfile11 ./peerplays/
docker build -t peerplays12 -f ./peerplays/Dockerfile12 ./peerplays/
docker build -t peerplays13 -f ./peerplays/Dockerfile13 ./peerplays/
docker build -t peerplays14 -f ./peerplays/Dockerfile14 ./peerplays/
docker build -t peerplays15 -f ./peerplays/Dockerfile15 ./peerplays/
docker build -t peerplays16 -f ./peerplays/Dockerfile16 ./peerplays/
